<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Listable
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/LocalBusiness">
			<?php

			if ( ! post_password_required() ) {
				$photos = listable_get_listing_gallery_ids();
				if ( ! empty( $photos ) ) : ?>

					<div class="entry-featured-carousel">
						<?php if ( count($photos) == 1 ):
							$myphoto = $photos[0];
							$image = wp_get_attachment_image_src($myphoto, 'listable-featured-image' );
							$src = $image[0];
						?>
							<div class="entry-cover-image" style="background-image: url(<?php echo listable_get_inline_background_image( $src ); ?>);"></div>
						<?php else: ?>
							<div class="entry-featured-gallery">
								<?php foreach ($photos as $key => $photo_id):
									$src = wp_get_attachment_image_src($photo_id, 'listable-carousel-image'); ?>
									<img class="entry-featured-image" src="<?php echo $src[0]; ?>" itemprop="image" />
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>

				<?php endif; ?>

				<div class="tix-section">
					<div class="buy-tix">
						<?php 

$posts = get_field('product_connect');

if( $posts ): ?>
    <ul>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li class="bartab">
        	<button class="guest_button">
            <a href="<?php the_permalink(); ?>">GET ON THE GUESTLIST</a>
        </button>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
<!-- 						<div class="tix-header">
							<h4>Get on the Guestlist</h4>
						</div> -->
<!-- 						

						<h3>When would you like to go?</h3>

						<div class = "date_selection_wrapper">
							<div class = "date_selection_input_wrapper" id = "month">
								<div class = "date_selected">
									<span>August</span>
								</div>
								<ul class = "date_options_list">
									<li>January</li>
									<li>February</li>
									<li>March</li>
									<li>April</li>
									<li>May</li>
									<li>June</li>
									<li>July</li>
									<li>August</li>
									<li>September</li>
									<li>October</li>
									<li>November</li>
									<li>December</li>
								</ul>
							</div>
							<div class = "date_selection_input_wrapper" id = "day">
								<div class = "date_selected">
									<span>1</span>
								</div>
								<ul class = "date_options_list">
									<li>1</li>
									<li>2</li>
									<li>3</li>
									<li>4</li>
									<li>5</li>
									<li>6</li>
									<li>7</li>
									<li>8</li>
									<li>9</li>
									<li>10</li>
									<li>11</li>
									<li>12</li>
									<li>13</li>
									<li>14</li>
									<li>15</li>
									<li>16</li>
									<li>17</li>
									<li>18</li>
									<li>19</li>
									<li>20</li>
									<li>21</li>
									<li>22</li>
									<li>23</li>
									<li>24</li>
									<li>25</li>
									<li>26</li>
									<li>27</li>
									<li>28</li>
									<li>29</li>
									<li>30</li>
									<li>31</li>
								</ul>
							</div>
							<div class = "date_selection_input_wrapper" id = "year">
								<div class = "date_selected">
									<span>2016</span>
								</div>
								<ul class = "date_options_list">
									<li>2016</li>
									<li>2017</li>
									<li>2018</li>
								</ul>
							</div>
						</div> --> <!-- date-selection-wrapper -->
<!-- 						<h3>Prebuy a bartab</h3>
					</div>
					<div class = "bar_tab_amount_selection_wrapper option_button_wrapper">
							<div class = "bar_tab_amount_option option_button option_button_selected">
								<span>$25 for $30</span>
								<input type="hidden" name="bar_tab_25" value="1" />
							</div>
							<div class = "bar_tab_amount_option option_button">
								<span>$50 for $60</span>
								<input type="hidden" name="bar_tab_50" value="0" />
							</div>
							<div class = "bar_tab_amount_option option_button">
								<span>$75 for $90</span>
								<input type="hidden" name="bar_tab_75" value="0" />
							</div>
							<div class = "bar_tab_amount_option option_button">
								<span>$100 for $120</span>
								<input type="hidden" name="bar_tab_100" value="0" />
							</div>
							<div class = "bar_tab_amount_option option_button">
								<span>$150 for $175</span>
								<input type="hidden" name="bar_tab_75" value="0" />
							</div>
							<div class = "bar_tab_amount_option option_button">
								<span>$200 for #250</span>
								<input type="hidden" name="bar_tab_100" value="0" />
							</div>
						</div>
						<button type = "submit" class = "button button-gold width_100" id = "submit_guestlist">
							<p>$200 worth of Drinks for $250 - <strong>BUY NOW</strong></p>
						</button>
				</div>

 -->
				<div>
					<?php
					$job_manager = $GLOBALS['job_manager'];

					remove_filter( 'the_content', array( $job_manager->post_types, 'job_content' ) );

					ob_start();

					do_action( 'job_content_start' );

					get_job_manager_template_part( 'content-single', 'job_listing' );

					do_action( 'job_content_end' );

					$content = ob_get_clean();

					add_filter( 'the_content', array( $job_manager->post_types, 'job_content' ) );

					echo apply_filters( 'job_manager_single_job_content', $content, $post );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'listable' ),
						'after'  => '</div>',
					) ); ?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php listable_entry_footer(); ?>
				</footer><!-- .entry-footer -->

			<?php
				listable_output_single_listing_icon();

			} else {
				echo '<div class="entry-content">';
				echo get_the_password_form();
				echo '</div>';
			} ?>
		</article><!-- #post-## -->

		<?php
		if ( ! post_password_required() ) the_post_navigation();
	endwhile; // End of the loop. ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>

<script type="text/javascript">
	//======= OPTION BUTTON =========//

	jQuery(".option_button").click(function(){
		var jQueryoption_button_wrapper = jQuery(this).closest(".option_button_wrapper");
		jQueryoption_button_wrapper.find(".option_button").removeClass("option_button_selected");
		var jQueryoption_button_inputs = jQueryoption_button_wrapper.find(".option_button").find("input");
		jQueryoption_button_inputs.val(0);
		jQuery(this).addClass("option_button_selected");
		jQuery('input', jQuery(this)).val(1);
	});


</script>

<style type="text/css">
.bartab {
	margin: 50px auto;
	background: #ff4d55;
	width: 50%;
	text-align: center;
	padding-top: 10px;
	padding-bottom: 10px;
	color:#fff;
	font-size: 18px;
}
</style>