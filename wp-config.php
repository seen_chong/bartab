<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bartab_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QQS,8}/sEHR8uxtzk.wk/D#PO)d~G|pH/&oHsa:CL#*zp&aSI~pBl`?Y)9ak8/}(');
define('SECURE_AUTH_KEY',  '(b26P!x%udI<V7!%@D4K6o#/7H$}[qQ}*.{Il2`szq2=I8C<Eib~&kjlt7lQ,r8z');
define('LOGGED_IN_KEY',    'U[VYS.zTN1H507wNw0klb=bRpPB0fDp)A&jHO9cWliQS<TjYo3IYxcrwz&eo`1Ui');
define('NONCE_KEY',        'EpU>SX/duaqaFyin*7.0}]qD5`&@j->r9|P24:vh$D;pxn_Nug?AJPJ20kh&~$Pw');
define('AUTH_SALT',        'eizN*fSww)|2NR:tad.+oDe+FMs6y&8k]HgPILba3uG?>g4tX@Y`; H$%B7lD]ND');
define('SECURE_AUTH_SALT', '({/z<uoC^@KvjT~oqMCyD&[WjOJ;$b`aSdC5_`DTm2(v,5tGiiJJ(K*9ZW/m_Eh-');
define('LOGGED_IN_SALT',   'et`k%+-<62a7+R]-^2[:j8h8@|QNpk,)H`YK5 Ec^.~2.2WK7:P}4z: Q{=uC#Q3');
define('NONCE_SALT',       '9LZf!<r8(0v~v8<&25;=[kL2X(iIc7)nR*4@>ov4gA#`wR(lrL)R_Ng^@LI//+K~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
